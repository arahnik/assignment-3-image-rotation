//
// Created by Abdujalol on 19.11.2023.
//

#ifndef BMP_H
#define BMP_H
#define BMP_HEADERS struct bmp_header
#include "image.h"
#include <stdio.h>

#pragma pack(push, 1)
BMP_HEADERS {
    uint16_t bfType;
    uint32_t  bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t  biHeight;
    uint16_t  biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t  biClrImportant;
};
#pragma pack(pop)

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
    READ_INVALID_FILE_SIZE
};

enum write_status {
    WRITE_OK = 0,
    WRITE_ERROR
};

enum read_status from_bmp(FILE* input, IMAGE* image);
enum write_status to_bmp(FILE* output, IMAGE const* image);

enum read_status handle_read_error(BMP_HEADERS);

#endif //BMP_H
