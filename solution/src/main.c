#include <stdio.h>

#include "bmp.h"
#include "helpers.h"

int main( int argc, char** argv ) {
    if (argc != 4) {
        perror("Expected 3 arguments: <\"inputfile\"> <\"outputfile\"> <\"angle\">");
        return -1;
    }

    FILE* in = fopen(argv[1], "rb");
    FILE* out = fopen(argv[2], "wb");

    if (!in || !out) {
	perror("Error while opening files");
	return -1;
    }

    IMAGE image = allocate_image(0, 0);

    const int angle = parse_int_from_string(argv[3]);
    if (angle % 90 != 0) {
        perror("Angle should be multiple of 90");
        return -1;
    }
    if (angle > 360 || angle < -360) {
	perror("Angle out of range ([-360; 360])");
	return -1;
    }
    const enum read_status status = from_bmp(in, &image);
    if (status != READ_OK) {
        perror("READING_ERROR");
        printf("\nSTATUS CODE: %d \n", status);
        return -1;
    }
    rotate(&image, angle);
    to_bmp(out, &image);
    free_image_data(&image);
    fclose(in);
    fclose(out);
    return 0;
}
